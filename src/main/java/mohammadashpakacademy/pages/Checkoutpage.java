package mohammadashpakacademy.pages;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import mohammadashpakacademy.abstractcomponents.Abstractcomponent;

public class Checkoutpage extends Abstractcomponent {
	public WebDriver driver;
	public Checkoutpage(WebDriver driver) {
		super(driver);
		this.driver= driver;
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "button[class*='3v1-ww']")
	WebElement addCartButton;
	

	@FindBy(css = "a[href*='viewcart']")
	WebElement goToCartButton;
	
	
	public void addToCart() {
		waitForWebElementVisible(addCartButton);
		addCartButton.click();
		
	}
	public Cartpage goToCart() {
		
		//we dont need this line, since when you click on add to cart it will automatically take you to carts page, 
		//please use this if its not taking you automatically to carts page
		//waitForWebElementVisible(goToCartButton);
		//goToCartButton.click();
		
		return new Cartpage(driver);
	}

}
