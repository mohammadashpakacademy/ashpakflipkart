package mohammadashpakacademy.pages;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import mohammadashpakacademy.abstractcomponents.Abstractcomponent;

public class Cartpage extends Abstractcomponent {
	public WebDriver driver;
	public Cartpage(WebDriver driver) {
		super(driver);
		this.driver= driver;
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath  = "//button[contains(@class,'_3AWRsL')]")
	WebElement placeOrderButton;
	

	@FindBy(xpath  = "//input[contains(@class,'_2IX_2')]")
	WebElement emailElement;
	
	@FindBy(xpath  = "//input[@class='_2IX_2- _17N0em']")
	WebElement phonElement;
	
	@FindBy(tagName  = "button")
	WebElement continueButton;
	
	public void  placeOrder()
	{
		waitForWebElementVisible(placeOrderButton);
		scrollInto(placeOrderButton);
		placeOrderButton.click();	
		
	}
	
	public void fillUsernamePhone(String email, String phone)
	{
		
		emailElement.sendKeys(email);
		
		waitForWebElementVisible(continueButton);
		continueButton.click();
		phonElement.sendKeys(phone);
		continueButton.click();
		
	}
	
	


}
