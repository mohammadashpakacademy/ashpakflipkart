package mohammadashpakacademy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.bidi.log.FilterBy;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import mohammadashpakacademy.abstractcomponents.Abstractcomponent;

public class Launchpage extends Abstractcomponent {
	WebDriver driver;
	
	public Launchpage(WebDriver driver) {
		
		super(driver);
		this.driver= driver;
		
		PageFactory.initElements(driver, this);
	}
	
@FindBy(css="button._3AWRsL")
WebElement loginElement;
@FindBy(css="div[class*='_2QfC02'] button")
WebElement closePopup;


@FindBy(css="input[name='q']")
WebElement searchbox;

@FindBy(css="div[class*='lrtEPN']")
WebElement searchElement;


@FindBy(css="button[type='submit']")
WebElement searchButton;


@FindBy(css="div[class='_4rR01T']")
WebElement firstElement;
	
	
public void launchUrl() {
		
		driver.get("https://www.flipkart.com/");
	}

public void closeLoginPopup() {
	
	waitForWebElementVisible(loginElement);
	if(loginElement.isDisplayed())
		closePopup.click();
		
	
}

public void searchElement(String productName) throws InterruptedException {
	
	searchbox.sendKeys(productName);
	
	//implemented thread sleep because as soon as we start typing i , imidiate results we are getting iphone, 
	//web element is grabing the text iphone and proceeding further, we cannot implement any explicit wait for this , hence adding
	//thread sleep so that list loads with correct data
	Thread.sleep(3000);
	//Implement streams
	searchElement.click();
	
	searchButton.click();
}

public Checkoutpage chooseFirstElement() {
	waitForWebElementVisible(firstElement);
	firstElement.click();
	return new Checkoutpage(driver);
}


}
