package mohammadashpakacademy.testcomponents;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.github.bonigarcia.wdm.WebDriverManager;
import mohammadashpakacademy.pages.Launchpage;

public class Base {
	protected WebDriver driver;

	public WebDriver initilizeBrowser() throws IOException {

		Properties properties = new Properties();

		FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir")
				+ "//src//main//java//mohammadashpakacademy//resources//GlobalData.properties");
		properties.load(fileInputStream);
		String browserName = properties.getProperty("browser");

		switch (browserName) {
		case "Chrome":
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();

			break;
		case "Edge":
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.manage().deleteAllCookies();

		return driver;
	}

	public Launchpage launchApplication() throws IOException {

		driver = initilizeBrowser();

		Launchpage lpageLaunchpage = new Launchpage(driver);
		lpageLaunchpage.launchUrl();
		return lpageLaunchpage;
	}

	public void switchToNextTab() {

		Set<String> window = driver.getWindowHandles();
		Iterator<String> it = window.iterator();
		String parentId = it.next();
		String childId = it.next();

		driver.switchTo().window(childId);
	}

	
	public void tearDown() {

		driver.quit();
	}

	public List<HashMap<String, String>> jsonDataParser(String filePath) throws IOException {

		String jsonContent = FileUtils.readFileToString(new File(filePath), StandardCharsets.UTF_8);

		ObjectMapper mapper = new ObjectMapper();

		List<HashMap<String, String>> dataHashMaps = mapper.readValue(jsonContent,
				new TypeReference<List<HashMap<String, String>>>() {
				});
		return dataHashMaps;

	}

}
