package mohammadashpakacademy.AshpakFlipkart;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import mohammadashpakacademy.pages.Cartpage;
import mohammadashpakacademy.pages.Checkoutpage;
import mohammadashpakacademy.pages.Launchpage;
import mohammadashpakacademy.testcomponents.Base;

public class Flipkartorder extends Base {

	@Test(dataProvider = "getData")
	public void orderPlacing(HashMap<String, String> input) throws IOException, InterruptedException {

		
		Launchpage launchpage = launchApplication();
		launchpage.closeLoginPopup();
		launchpage.searchElement(input.get("product"));

//Here unable to perform "on results page, filter all results by the Online Only filter" , i dont find such filter in webpage

		// wait for results and click
		Checkoutpage checkoutPage = launchpage.chooseFirstElement();

		// switching control to new tab , results are loaded in new tab
		switchToNextTab();

		// add to cart
		checkoutPage.addToCart();
		Cartpage cartpage = checkoutPage.goToCart();

		// placeOrder
		cartpage.placeOrder();
		// provide email and phone
		cartpage.fillUsernamePhone(input.get("email"), input.get("phone"));
		
		tearDown();
	}
	
	@DataProvider
	public Object getData() throws IOException
	{
		

		List<HashMap<String, String>> dataHashMaps =jsonDataParser(System.getProperty("user.dir")+"//src//test//java//mohammadashpakacademy//data//userData.json");

		return new Object[][]{{dataHashMaps.get(0)}};
		
	}
	
}
